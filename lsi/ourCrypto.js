// Nodejs encryption with CTR
var crypto = require('crypto');
var params = require('../params.js');
//params



var encrypt=function (text){
  var cipher = crypto.createCipher(params.algorithm,params.password)
  var crypted = cipher.update(text,'utf8','hex')
  crypted += cipher.final('hex');
  return crypted;
};

var decrypt=function (text){
  var decipher = crypto.createDecipher(params.algorithm,params.password)
  var dec = decipher.update(text,'hex','utf8')
  dec += decipher.final('utf8');
  return dec;
};

//console.log(decrypt(encrypt('key1')));

var encryptKeyVal=function (key, value){
  //var cipher = crypto.createCipher(algorithm,key)
  var key32bit = new Buffer(32);
  //console.log(typeof key);
  if(typeof key === 'number')  {
    key32bit.write(key.toString());
  }
  else {
    key32bit.write(key);
  }
  var cipher = crypto.createCipheriv(params.algorithm,key32bit,params.PrivareKeyPSBbtIV)
  var crypted = cipher.update(value,'utf8','hex')
  crypted += cipher.final('hex');
  return crypted;
};
//
var decryptKeyVal=function (key, value){
  //var cipher = crypto.createCipher(algorithm,key)
  var key32bit = new Buffer(32);
  if(typeof key === 'number')  {
    key32bit.write(key.toString());
  }
  else {
    key32bit.write(key);
  }
  var decipher = crypto.createDecipheriv(params.algorithm,key32bit,params.PrivareKeyPSBbtIV)
  var decrypted = decipher.update(value,'hex','utf8')
  decrypted += decipher.final('utf8');
  return decrypted;
};

//console.log(decryptKeyVal('key',encryptKeyVal('key','text')));

//privateKeybtIV 16 bit publicKey 32 bit
var encryptKeyValPrivatePublic=function (privateKeybtIV, publicKey, value){
  //var cipher = crypto.createCipher(algorithm,key)
  var key32bit = new Buffer(32);
  //console.log(typeof key);
  if(typeof publicKey === 'number')  {
    key32bit.write(publicKey.toString());
  }
  else {
    key32bit.write(publicKey);
  }
  var cipher = crypto.createCipheriv(params.algorithm,key32bit,privateKeybtIV)
  var crypted = cipher.update(value,'utf8','hex')
  crypted += cipher.final('hex');
  return crypted;
};

var decryptKeyValPrivatePublic=function (privateKeybtIV, publicKey, value){
  //var cipher = crypto.createCipher(algorithm,key)
  var key32bit = new Buffer(32);
  if(typeof publicKey === 'number')  {
    key32bit.write(publicKey.toString());
  }
  else {
    key32bit.write(publicKey);
  }
  var decipher = crypto.createDecipheriv(algorithm,key32bit,privateKeybtIV)
  var decrypted = decipher.update(value,'hex','utf8')
  decrypted += decipher.final('utf8');
  return decrypted;
};

var text2Hash=function(text)
{
  var hash, hmac;
  //var keyHash=crypto.randomBytes(32);
  hmac = crypto.createHmac(params.algorithmHash, params.keyHash);
  hmac.update(text);
  hash = hmac.digest('hex');
  return hash;
  //console.log("Method 2: ", hash);
}

// var encryptKeyValJSON=function (key32bit, jsonData){
//   //var cipher = crypto.createCipher(algorithm,key)
//   for(var prop in jsonData){
//       if(jsonData.hasOwnProperty(prop)){
//           console.log(prop + ':' + jsonData[prop]);
//           //jsonData[prop]=encryptKeyVal(key32bit, )
//       }
//   }
// };

// var prime_length = 60;
//
// generateKeys=function()
// {
//   //console.log("Start Generate keys");
//   var diffHell;
//   diffHell = crypto.createDiffieHellman(prime_length);
//
//   diffHell.generateKeys('base64');
//   return diffHell;
//   //console.log("Public Key : " ,diffHell.getPublicKey('base64'));
//   //console.log("Private Key : " ,diffHell.getPrivateKey('base64'));
//
//   console.log("Public Key : " ,diffHell.getPublicKey('hex'));
//   console.log("Private Key : " ,diffHell.getPrivateKey('hex'));
// }

var encryptStringWithRsaPublicKey = function(toEncrypt, publicKey) {
    //var absolutePath = path.resolve(relativeOrAbsolutePathToPublicKey);
    //var publicKey = fs.readFileSync(absolutePath, "utf8");
    var buffer = new Buffer(toEncrypt);
    var encrypted = crypto.publicEncrypt(publicKey, buffer);
    return encrypted.toString("base64");
};

var decryptStringWithRsaPrivateKey = function(toDecrypt, privateKey) {
    //var absolutePath = path.resolve(relativeOrAbsolutePathtoPrivateKey);
    //var privateKey = fs.readFileSync(absolutePath, "utf8");
    var buffer = new Buffer(toDecrypt, "base64");
    var decrypted = crypto.privateDecrypt(privateKey, buffer);
    return decrypted.toString("utf8");
};

/*
Generating random numbers in specific range using crypto.randomBytes from crypto library
Maximum available range is 281474976710655 or 256^6-1
Maximum number for range must be equal or less than Number.MAX_SAFE_INTEGER (usually 9007199254740991)
Usage examples:
cryptoRandomNumber(0, 350);
cryptoRandomNumber(556, 1250425);
cryptoRandomNumber(0, 281474976710655);
cryptoRandomNumber((Number.MAX_SAFE_INTEGER-281474976710655), Number.MAX_SAFE_INTEGER);

Tested and working on 64bit Windows and Unix operation systems.
*/

// var cryptoRandomNumber=function (minimum, maximum){
// 	var distance = maximum-minimum;
//
// 	if(minimum>=maximum){
// 		console.log('Minimum number should be less than maximum');
// 		return false;
// 	} else if(distance>281474976710655){
// 		console.log('You can not get all possible random numbers if range is greater than 256^6-1');
// 		return false;
// 	} else if(maximum>Number.MAX_SAFE_INTEGER){
// 		console.log('Maximum number should be safe integer limit');
// 		return false;
// 	} else {
// 		var maxBytes = 6;
// 		var maxDec = 281474976710656;
//
// 		// To avoid huge mathematical operations and increase function performance for small ranges, you can uncomment following script
// 		/*
// 		if(distance<256){
// 			maxBytes = 1;
// 			maxDec = 256;
// 		} else if(distance<65536){
// 			maxBytes = 2;
// 			maxDec = 65536;
// 		} else if(distance<16777216){
// 			maxBytes = 3;
// 			maxDec = 16777216;
// 		} else if(distance<4294967296){
// 			maxBytes = 4;
// 			maxDec = 4294967296;
// 		} else if(distance<1099511627776){
// 			maxBytes = 4;
// 			maxDec = 1099511627776;
// 		}
// 		*/
//
// 		var randbytes = parseInt(crypto.randomBytes(maxBytes).toString('hex'), 16);
// 		var result = Math.floor(randbytes/maxDec*(maximum-minimum+1)+minimum);
//
// 		if(result>maximum){
// 			result = maximum;
// 		}
// 		return result;
// 	}
// }

//GenerateKey();
//var PublicKey='a27530b7a5c192da31f3717df501caef';
//var PrivateKey='757bc7f00c7207ea8495969121bfde40';
//var aPrime='d568ceaf2f05e7dc73b0bdcba2f862bb';
//var aPrimeSize=16;
//var aGenerator='02';
//var aGeneratorSize=1;

var GenerateSecretByKey=function (publicKey)
{
  //Buffer Prime32
  var bPrime=new Buffer(params.aPrime, 'hex');
  var bGenerator=new Buffer(params.aGenerator, 'hex');

  //var key1='d568';
  var key16bit = new Buffer(16);
  //console.log(typeof key);
  if(typeof publicKey === 'number')  {
    key16bit.write(publicKey.toString());
  }
  else {
    key16bit.write(publicKey);
  }

  var diffHellA, diffHellB;
  diffHellA = crypto.createDiffieHellman(bPrime, bGenerator);
  diffHellA.setPrivateKey(params.PrivateKey,'hex');
  diffHellA.setPublicKey(key16bit,'hex');

  diffHellB = crypto.createDiffieHellman(bPrime, bGenerator);
  diffHellB.setPrivateKey(params.PrivateKey,'hex');
  diffHellB.setPublicKey(key16bit,'hex');

  const keySecretA = diffHellA.computeSecret(diffHellB.getPublicKey(), null, 'hex');
  //const keySecretB = diffHellB.computeSecret(diffHellA.getPublicKey(), null, 'hex');
  //console.log("Secret Key A: " ,keySecretA);
  //console.log("Secret Key B: " ,keySecretB);
  return keySecretA;
}

//module.exports.testCrypto=testCrypto;
//module.exports.testCryptoKeyVal=testCryptoKeyVal;
module.exports.encryptKeyVal=encryptKeyVal;
module.exports.decryptKeyVal=decryptKeyVal;
module.exports.text2Hash=text2Hash;
//module.exports.generateKeys=generateKeys;
//module.exports.encryptStringWithRsaPublicKey=encryptStringWithRsaPublicKey;
//module.exports.decryptStringWithRsaPrivateKey=decryptStringWithRsaPrivateKey;
//module.exports.cryptoRandomNumber=cryptoRandomNumber;
//module.exports.decryptKeyValPrivatePublic=decryptKeyValPrivatePublic;
module.exports.encryptKeyValPrivatePublic=encryptKeyValPrivatePublic;
module.exports.GenerateSecretByKey=GenerateSecretByKey;
module.exports.encrypt=encrypt;
module.exports.decrypt=decrypt;
module.exports.GenerateKey=GenerateKey;

//Start tests
// var testCryptoKeyVal=function (key_text, text){
//   //var key32bit = new Buffer(32);
//   //key32bit.write(key_text);
//   //console.log(key32bit);
//   //console.log(key32bit.length);
//   console.log('g');
//   var hw = encryptKeyVal(key_text, text)
//   // outputs hello world
//   console.log(hw);
//   var dw=decryptKeyVal(key_text, hw);
//   console.log(dw);
// };
//
// var jsonDataObj = [{"person":"me","age":"30"},{"person":"you","age":"25"}];
// var jsonData = {"person":"you","age":"25"};
// var testJSONObj=function(jsonData)
// {
//   for(var obj in jsonData){
//       if(jsonData.hasOwnProperty(obj)){
//       for(var prop in jsonData[obj]){
//           if(jsonData[obj].hasOwnProperty(prop)){
//               console.log(prop + ':' + jsonData[obj][prop]);
//           }
//       }
//   }
//   }
//     //console.log(e);
// }

// var testJSON=function(jsonData)
// {
//       for(var prop in jsonData){
//           if(jsonData.hasOwnProperty(prop)){
//               console.log(prop + ':' + jsonData[prop]);
//           }
//       }
//     //console.log(e);
// }

// var testCrypto=function (){
// var hw = encrypt("hello world crypto")
// // outputs hello world
// console.log(decrypt(hw));
// };

function GenerateKey()
{
  var diffHell;
  diffHell = crypto.createDiffieHellman(128);
  diffHell.generateKeys('hex');
  //return diffHell;
  //console.log("Public Key : " ,diffHell.getPublicKey('base64'));
  //console.log("Private Key : " ,diffHell.getPrivateKey('base64'));

  //console.log("Public Key : " ,diffHell.getPublicKey('hex'));
  console.log("Private Key : " ,diffHell.getPrivateKey('hex'));
  console.log("Prime : " ,diffHell.getPrime());
  //console.log("Prime : " ,diffHell.getPrime().length);
  console.log("Generator : " ,diffHell.getGenerator());
  //console.log("Generator : " ,diffHell.getGenerator().length);
}

function GenerateSecretTest()
{
  //Buffer Prime32
  var bPrime=new Buffer(aPrime, 'hex');
  var bGenerator=new Buffer(aGenerator, 'hex');

  var key1='d568';
  var diffHellA, diffHellB;
  diffHellA = crypto.createDiffieHellman(bPrime, bGenerator);
  diffHellA.setPrivateKey(PrivateKey,'hex');
  diffHellA.setPublicKey(key1,'hex');

  diffHellB = crypto.createDiffieHellman(bPrime, bGenerator);
  diffHellB.setPrivateKey(PrivateKey,'hex');
  diffHellB.setPublicKey(key1,'hex');

  const keySecretA = diffHellA.computeSecret(diffHellB.getPublicKey(), null, 'hex');
  const keySecretB = diffHellB.computeSecret(diffHellA.getPublicKey(), null, 'hex');
  console.log("Secret Key A: " ,keySecretA);
  console.log("Secret Key B: " ,keySecretB);
}

//GenerateSecretByKey('a199');
//GenerateSecretTest();

//console.log(crypto.getPrime());
//console.log(text2Hash('Test to hash'));
//testJSON(jsonData);

//testCryptoKeyVal('key1', 'test to crypt 1');
//testCryptoKeyVal(9999, 'test to crypt 2');
//console.log(cryptoRandomNumber(1,99999));
//end tests
