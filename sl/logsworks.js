function create_log(sessionID, clientID, referrer, host, eventID, info)
{
    return {
        SessionID: sessionID,
        ClientID: clientID,
        Referrer: referrer,
        Host: host,
        EventId: eventID,
        Info: info,
        DetailedDateTime:new Date().toISOString()
    };
}

function createlogs(Sessionid,ClientID,Referrer,Host,Action,Severety,Module,Info,ExposedToUser){
      return {
        EventId:getId({
          Module:Module,
          Severity:Severety,
          ExposedToUser:ExposedToUser,
          Action:Action
        }),
        SessionID:Sessionid,
        ClientID: ClientID,
        Referrer:Referrer,
        Host:Host,
        Info:Info,
        DetailedDateTime:new Date().toISOString()
      }
    }
function getId(logDB){
      var EventId ='';
      for(var key in logDB){
        if(key == 'Module'){
          switch(logDB[key]){
            case 'Client':
              EventId += '1';
              break;
            case 'A':
              EventId += '2';
              break;
            case 'B':
              EventId += '3';
              break;
            case 'PSB':
              EventId += '4';
              break;
            case 'UI actions':
              EventId += '5';
              break;
            default :
              break;
          }
        } else if(key == 'Severity'){
          switch(logDB[key]){
            case 'Info':
              EventId += '1';
              break;
            case 'Warning':
              EventId += '2';
              break;
            case 'Error':
              EventId += '3';
              break;
            case 'Critical':
              EventId += '4';
              break;
            default :
              break;
          }
        } else if(key == 'ExposedToUser'){
          switch(logDB[key]){
            case 'false':
              EventId += '0';
              break;
            case 'true':
              EventId += '1';
              break;
            default :
              break;
          }
        } else if(key == 'Action'){
          switch(logDB[key]){
            case 'Allow':
              EventId += '10';
              break;
            case 'Deny':
              EventId += '11';
              break;
            case 'Block':
              EventId += '20';
              break;
            case 'Forward':
              EventId += '21';
              break;
            case 'Modify Consent':
              EventId += '30';
              break;
            case 'UI action':
              EventId += '40';
              break;
            default :
              break;
          }
        }
      }
      return parseInt(EventId);
    }

module.exports.create_log = create_log;
module.exports.createlogs=createlogs;
module.exports.getId=getId;
