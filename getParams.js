//const crypto = require('crypto');
const fs=require('fs');

//var prime = crypto.createDiffieHellman(128).getPrime('hex');
var json = JSON.parse(fs.readFileSync('Settings.json', 'utf8'));

var pem = fs.readFileSync('PrivateKey.pem', 'utf8');
    res = pem.split('\n');
    bool = false;
    key = "";
res.forEach(function (item, inex){
  if (item.match(/-----end/i))
      bool = false;
  else if (bool)
      key += item;
  else if (item.match(/-----begin/i))
      bool = true;
});

var len = key.length,
    cnt = Math.round(len/3 - 0.5);
    cnt2 = Math.round(len/16 - 0.5);

var password = key.slice(0, cnt);
var keyHash = key.slice(cnt, cnt*2);
var PrivateKey= key.slice(cnt*2, cnt*3);

var i, hex;
var arr = [];
for (i = 0; i < 16; i++) {
  hex = key.charCodeAt(cnt2 + cnt2*i).toString(16);
  arr.push("0x" + hex);
}
// var iv = new Buffer(arr)
//   var ivstring = iv.toString('hex').slice(0, 16);

// 'var key = \"' + key + "\";" + "\n" + "var prime = \"" + prime + "\";";
var txt = '//for encrypt and decrypt fields value and more' + '\n' +
          'var password = \"' + password + '\";' + '\n' +
          'var algorithm = \"'+ json.algorithmCipher + '\";' + '\n' +

          '//for Hash' + '\n' +
          'var keyHash = \"' + keyHash + '\"; //make this your secret!!' + '\n' +
          'var algorithmHash = \"' + json.algorithmHash + '\"; //"sha1";   //consider using sha256' + '\n' +

          '//for GenerateSecretByKey  and encryptKeyVal or decryptKeyVal for dppKey and dppValue' + '\n' +
          '//this key from GenerateKey()' + '\n' +
          'var PrivateKey = \"' + PrivateKey + '\";' + '\n' +
          'var aPrime = \"' + json.primeNumber + '\";' + '\n' +
          'var aGenerator = "02";' + '\n' +

          '//for PSB' + '\n' +
          'var PrivareKeyPSBbtIV = new Buffer('+ arr + '); //16bit' + '\n' +

          'exports.password=password;' + '\n' +
          'exports.algorithm=algorithm;' + '\n' +
          'exports.keyHash=keyHash;' + '\n' +
          'exports.algorithmHash=algorithmHash;' + '\n' +
          'exports.PrivateKey=PrivateKey;' + '\n' +
          'exports.aPrime=aPrime;' + '\n' +
          'exports.aGenerator=aGenerator;' + '\n' +
          'exports.PrivareKeyPSBbtIV=PrivareKeyPSBbtIV;' + '\n'
//console.log(txt);
fs.writeFileSync("params.js", txt);
